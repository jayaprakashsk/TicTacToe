var i = 1
let matrix = document.getElementsByClassName('box')
let array;
matrix = [[' ',' ',' '],[' ',' ',' '],[' ',' ',' ']]
let turn = true;
let flag = false;

let isLeftToRightDiagonalWins = function()
{
  for(let i = 0 ; i<2 ; i++)
  {

    if(matrix[i][i] != matrix[i+1][i+1]  || matrix[i][i] == ' ' || matrix[i+1][i+1] == ' ')
    {
      return false;
    }
  }
    return true;
}



let isRighttoLeftDiagonalWins = function()
{
  let j = 0,k=2;
  for(let i = 0 ; i<2 ; i++)
  {
      if(matrix[j][k] != matrix[j+1][k-1] || matrix[j][k] == ' ' || matrix[j+1][k-1] == ' '){
          return false;
    }
    j++;
    k--;
  }
  return true;
}
let columnWins = function(column){

    for(let i = 0 ; i<2 ; i++)
    {
      if(matrix[i][column] != matrix[i+1][column] || matrix[i][column] == ' ' || matrix[i+1][column] == ' ' )
      {
        return false;
      }
    }
    return true;
}

let rowWins = function(row){

    for(let i = 0 ; i<2 ; i++)
    {
      if((matrix[row][i] != matrix[row][i+1]) || matrix[row][i] == ' ' || matrix[row][i+1] == ' ')
      {
        return false;
      }
    }
    return true;
}


let isWin = function(index){

  if(rowWins(parseInt(index[0])) || columnWins(parseInt(index[1])) || isLeftToRightDiagonalWins() || isRighttoLeftDiagonalWins())
  {

      return true;
  }

    // if( objects['00'] == objects['01'] && objects['02']== objects['01'] && objects['02']!=' ')
    // {
    //   console.log(objects['00'] + objects['01'] + objects['02']+ objects['01'] + objects['02']);
    //   return true;
    // }
    // if( objects['10'] == objects['11'] && objects['12'] == objects['11'] && objects['12']!=' '){
    //   console.log(objects['10'] + objects['11'] + objects['12']+ objects['11'] + objects['12']);
    //   return true;
    // }
    // if(objects['20'] == objects['21'] && objects['22']== objects['21'] && objects['22']!=' ')
    // {
    //   console.log(objects['20'] + objects['21'] + objects['22']+ objects['21'] + objects['22']);
    //   return true;
    // }
    //
    // if(objects['00'] == objects['10'] &&  objects['20'] == objects['10'] && objects['20'] != ' ' )
    // {
    //   console.log(objects['00'] + objects['10'] + objects['20']+ objects['10'] + objects['20']);
    //   return true;
    // }
    //
    // if(objects['01'] == objects['11'] && objects['21'] == objects['11']&& objects['21']!=' ')
    // {
    //   console.log(objects['01'] + objects['11'] + objects['21']+ objects['11'] + objects['21']);
    //   return true;
    // }
    // if(objects['02'] == objects['12'] && objects['22'] == objects['12'] && objects['22']!=' ')
    // {
    //   console.log(objects['02'] + objects['12'] + objects['22']+ objects['12'] + objects['22']);
    //     return true;
    // }
    //
    // if(objects['00'] == objects['11'] && objects['22'] == objects['11'] && objects['22']!=' ')
    // {
    //   console.log(objects['00'] + objects['11'] + objects['22']+ objects['11'] + objects['22']);
    //     return true;
    // }
    //
    // if(objects['02'] == objects['11'] && objects['20']  == objects['11'] && objects['20']!=' ')
    // {
    //   console.log(objects['02'] + objects['11'] + objects['20']+ objects['11'] + objects['20']);
    //     return true;
    // }

    return false;
}



for(let i of matrix)
{
  for(let j in i)
  {
    console.log(i+'   '+j);
  }
}


let onClick = function(event){
if(!flag){
  if(turn && matrix[parseInt(event.target.id[0])][parseInt(event.target.id[1])] == ' ')
  {
    document.getElementById(event.target.id).innerHTML = 'X';
      matrix[parseInt(event.target.id[0])][parseInt(event.target.id[1])] = 'X';

      if(isWin(event.target.id))
      {
        flag = true;
        document.getElementById('caption').innerHTML = 'X turn';
        setTimeout(function(){
        alert("X wins");
        location.reload(true);
      },250);

      }
    turn = false;
    document.getElementById('caption').innerHTML = 'O turn';
  }
  else if(!turn  && matrix[parseInt(event.target.id[0])][parseInt(event.target.id[1])] == ' '){
    document.getElementById(event.target.id).innerHTML = 'O';
    matrix[parseInt(event.target.id[0])][parseInt(event.target.id[1])] = 'O';

    if(isWin(event.target.id))
    {
      document.getElementById('caption').innerHTML = 'O wins';
      flag = true;

      setTimeout(function(){
          alert("O wins");
        location.reload(true);
      },250);
    }
    turn = true;
    document.getElementById('caption').innerHTML = 'X turn';
  }
}
setTimeout(function(){
    if(matrix[0][0]!=' ' && matrix[0][1]!=' '&& matrix[0][2]!=' '&& matrix[1][0]!=' '&& matrix[1][1]!=' '&& matrix[1][2]!=' '&& matrix[2][0]!=' '&& matrix[2][1]!=' ' && matrix[2][2]!=' ')
      {
        flag = true;
        alert("Match drawn...");
        location.reload(true);
}
},300);

}
